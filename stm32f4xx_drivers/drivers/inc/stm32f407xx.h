/*
 * stm32f407xx.h
 *
 *  Created on: Aug 26, 2019
 *      Author: gabri_000
 */

#ifndef INC_STM32F407XX_H_
#define INC_STM32F407XX_H_

#include <stdint.h>
/*
 * Base address of flash and sram memories
 */

#define FLASH_BASEADDR					0x08000000U
#define SRAM1_BASEADDR					0x20000000U  //112KB
#define SRAM2_BASEADDR					0x2001C000U
#define ROM_BASEADDR					  0x1FFF0000U  //30KB
#define SRAM 							      SRAM1_BASEADDR

/*
 * System Buses
 */

#define PERIPH_BASE						  0x40000000U
#define APB1PERIPH_BASE					PERIPH_BASE
#define APB2PERIPH_BASE					0x40010000U
#define AHB1PERIPH_BASE					0x40020000U
#define AHB2PERIPH_BASE					0x50000000U

/*
 * Base address of AHB1 bus periphs
 */

#define GPIOA_BASE					(AHB1PERIPH_BASE + 0x0000)
#define GPIOB_BASE					(AHB1PERIPH_BASE + 0x0400)
#define GPIOC_BASE					(AHB1PERIPH_BASE + 0x0800)
#define GPIOD_BASE					(AHB1PERIPH_BASE + 0x0C00)
#define GPIOE_BASE					(AHB1PERIPH_BASE + 0x1000)
#define GPIOF_BASE					(AHB1PERIPH_BASE + 0x1400)
#define GPIOG_BASE					(AHB1PERIPH_BASE + 0x1800)
#define GPIOH_BASE					(AHB1PERIPH_BASE + 0x1C00)
#define GPIOI_BASE					(AHB1PERIPH_BASE + 0x2000)

/*
 * Base address of APB1 bus periphs
 */

#define I2C1_BASE					  (APB1PERIPH_BASE + 0x5400)
#define I2C2_BASE					  (APB1PERIPH_BASE + 0x5800)
#define I2C3_BASE					  (APB1PERIPH_BASE + 0x5C00)
#define SPI2_BASE					  (APB1PERIPH_BASE + 0x5800)
#define SPI3_BASE					  (APB1PERIPH_BASE + 0x5800)
#define USART2_BASE					(APB1PERIPH_BASE + 0x4400)
#define USART3_BASE					(APB1PERIPH_BASE + 0x4800)
#define UART4_BASE					(APB1PERIPH_BASE + 0x4C00)
#define UART5_BASE					(APB1PERIPH_BASE + 0x5000)

/*
 * Base address of APB2 bus periphs
 */

#define EXTI_BASE   					  (APB2PERIPH_BASE + 0x3C00)
#define SPI1_BASE   					  (APB2PERIPH_BASE + 0x5800)
#define USART1_BASE   					(APB2PERIPH_BASE + 0x1000)
#define USART6_BASE   					(APB2PERIPH_BASE + 0x1400)
#define SYSCFG_BASE   					(APB2PERIPH_BASE + 0x3800)

/*********************** periph register def structures *************************/

/*
 * periph register struct for GPIO
 */

typedef struct
{
	volatile uint32_t	MODER;
	volatile uint32_t	OTYPER;
	volatile uint32_t	OSPEEDR;
	volatile uint32_t	PUPDR;
	volatile uint32_t	IDR;
	volatile uint32_t	ODR;
	volatile uint32_t	BSRRL;
	volatile uint32_t	BSRRH;
	volatile uint32_t	LCKR;
	volatile uint32_t	AFR[2];
} GPIO_RegDef_t;

typedef struct
{
	volatile uint32_t CR;
	volatile uint32_t PLLCFGR;
	volatile uint32_t CFGR;
	volatile uint32_t CIR;
	volatile uint32_t AHB1RSTR;
	volatile uint32_t AHB2RSTR;
	volatile uint32_t AHB3RSTR;
	uint32_t RESERVED1;
	volatile uint32_t APB1RSTR;
	volatile uint32_t APB2RSTR;
	uint32_t RESERVED2[2];
	volatile uint32_t AHB1ENR;
	volatile uint32_t AHB2ENR;
	volatile uint32_t AHB3ENR;
	uint32_t RESERVED3;
	volatile uint32_t APB1ENR;
	volatile uint32_t APB2ENR;
	uint32_t RESERVED4[2];
	volatile uint32_t AHB1LPENR;
	volatile uint32_t AHB2LPENR;
	volatile uint32_t AHB3LPENR;
	uint32_t RESERVED5;
	volatile uint32_t APB1LPENR;
	volatile uint32_t APB2LPENR;
	uint32_t RESERVED6[2];
	volatile uint32_t BDCR;
	volatile uint32_t CSR;
	uint32
} RCC_RegDef_t;

#define GPIOA		((GPIO_RegDef_t*)GPIOA_BASE)
#define GPIOB		((GPIO_RegDef_t*)GPIOB_BASE)
#define GPIOC		((GPIO_RegDef_t*)GPIOC_BASE)
#define GPIOD		((GPIO_RegDef_t*)GPIOD_BASE)
#define GPIOE		((GPIO_RegDef_t*)GPIOE_BASE)
#define GPIOF		((GPIO_RegDef_t*)GPIOF_BASE)
#define GPIOG		((GPIO_RegDef_t*)GPIOG_BASE)
#define GPIOH		((GPIO_RegDef_t*)GPIOH_BASE)
#define GPIOI		((GPIO_RegDef_t*)GPIOI_BASE)

GPIO_RegDef_t *pGPIOA = GPIOA;

#endif /* INC_STM32F407XX_H_ */
